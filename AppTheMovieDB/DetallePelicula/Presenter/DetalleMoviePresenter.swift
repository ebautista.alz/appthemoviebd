//
//  DetalleMoviePresenter.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 17/02/23.
//

import Foundation

class DetalleMoviePresenter: DetalleMoviePresenterDelegate{
    
    var view: DetalleMovieViewDelegate?
    var interactor: DetalleMovieInteractorDelegate?

    init(interface: DetalleMovieViewDelegate, interactor: DetalleMovieInteractorDelegate) {
        self.view = interface
        self.interactor = interactor
    }
    
   
    func getDetalleMovie(_ idMovie: Int){
        interactor?.requestDetalleMovie(idMovie)
    }

    func successDetalleMovie(_ detalle: Movies){
        view?.updateInfoMovie(detalle)
    }

    func errorDetalleMovie(_ msg: String){
        
    }
    
    func searchStatusFavMovie(_ idMovie: Int){
        interactor?.searchFav(idMovie)
    }
    
    func showStatusFavMovie(_ movie: MoviesFav){
        view?.successSerchFav(movie)
    }
    
    func addFavorito(_ movie: Movies){
        interactor?.addFavorito(movie)
    }
    
    func removeFavorito(_ movie: MoviesFav){
        interactor?.eliminarFavorito(movie)
        
    }
    
    func updateStatusBtnFav(_ delete: Bool){
        view?.updateStatusBtn(delete)
    }
}
