//
//  DetalleMovieRouter.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 17/02/23.
//

import UIKit

class DetalleMovieRouter: DetalleMovieRouterDelegate {
    
    weak var viewController: UIViewController?
    
    static func createModule(idMovie: Int) -> UIViewController {
        let view = DetalleMovieViewController(nibName: "DetalleMovieViewController", bundle: Bundle(for: DetalleMovieViewController.self))
        view.idMovie = idMovie
        let interactor = DetalleMovieInteractor()
        let presenter = DetalleMoviePresenter(interface: view, interactor: interactor)
        view.presenter = presenter
        interactor.presenter = presenter
        return view
    }
    
}
