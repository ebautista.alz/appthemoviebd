//
//  DetalleMovieProtocol.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 17/02/23.
//

import Foundation

protocol DetalleMovieViewDelegate: AnyObject {
    var presenter:  DetalleMoviePresenterDelegate? { get set }
    func updateInfoMovie(_ detalleMovie: Movies)
    func successSerchFav(_ movie: MoviesFav)
    func updateStatusBtn(_ isDelete: Bool)
}
protocol DetalleMoviePresenterDelegate: AnyObject {
    func getDetalleMovie(_ idMovie: Int)
    func successDetalleMovie(_ detalle: Movies)
    func errorDetalleMovie(_ msg: String)
    func searchStatusFavMovie(_ idMovie: Int)
    func showStatusFavMovie(_ movie: MoviesFav)
    func updateStatusBtnFav(_ delete: Bool)
    func addFavorito(_ movie: Movies)
    func removeFavorito(_ movie: MoviesFav)
}
protocol DetalleMovieInteractorDelegate: AnyObject {
    var presenter:  DetalleMoviePresenterDelegate? { get set }
    func requestDetalleMovie(_ idMovie: Int)
    func addFavorito(_ movie: Movies)
    func eliminarFavorito(_ movie: MoviesFav)
    func searchFav(_ idMovie: Int)
}
protocol DetalleMovieRouterDelegate: AnyObject {}
