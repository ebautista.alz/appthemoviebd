//
//  DetalleMovieViewController.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 17/02/23.
//

import UIKit

class DetalleMovieViewController: BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescripcion: UILabel!
    @IBOutlet weak var lblGenero: UILabel!
    @IBOutlet weak var lblIdioma: UILabel!
    @IBOutlet weak var lblCalificacion: UILabel!
    @IBOutlet weak var lblVotos: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDuracion: UILabel!
    @IBOutlet weak var imgPortada: UIImageView!
    @IBOutlet weak var btnFavs: UIButton!

    @IBOutlet weak var galleryComponent: GalleryComponent!
    @IBOutlet weak var lblTitleGallery: UILabel!

    var idMovie: Int = 0
    var presenter:  DetalleMoviePresenterDelegate?
    private let loader = Utils.showLoader()
    private var detalleMovie: Movies?
    private var favMovie: MoviesFav?

 
    private var favoritos: [MoviesFav] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Detalle pelicula"
        lblTitle.textColor = .greenLime
        lblCalificacion.textColor = .greenLime
        lblVotos.textColor = .greenLime
        lblDate.textColor = .greenLime
        lblGenero.textColor = .greenLime
        lblIdioma.textColor = .greenLime
        lblDuracion.textColor = .greenLime
        lblTitleGallery.textColor = .greenLime
        presenter?.getDetalleMovie(idMovie)
        showBackButton()
        presenter?.searchStatusFavMovie(idMovie)
        
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        galleryComponent.removeTimer()
    }
    
    @IBAction func addFavorito(_ sender: UIButton) {
        if let movie = favMovie{
            presenter?.removeFavorito(movie)
        }else{
            presenter?.addFavorito(detalleMovie!)
        }
        
    }
    
}

extension DetalleMovieViewController: DetalleMovieViewDelegate {
    func updateInfoMovie(_ detalleMovie: Movies){
        self.detalleMovie = detalleMovie
        lblTitle.text = detalleMovie.title ?? ""
        lblDescripcion.text = detalleMovie.overview ?? ""
        let icon = UIImage(named: "startRate") ?? UIImage()
        lblCalificacion.attributedText = Utils.addIconLabel(image: icon, text: "\(detalleMovie.voteAverage ?? 0.0)")
        lblVotos.text = "\(detalleMovie.voteCount ?? 0)"
        lblDate.text = detalleMovie.releaseDate ?? ""
        lblGenero.text = getGeneros(detalleMovie.genres ?? [])
        lblIdioma.text = getOriginalLanguage(detalleMovie.originalLanguage ?? "")
        lblDuracion.text = "\(detalleMovie.runtime ?? 0) min"
        Utils.getImageFromUrl("https://image.tmdb.org/t/p/w500\(detalleMovie.backdropPath ?? "")", completion: { image in
            self.imgPortada.image = image
            self.imgPortada.contentMode = .scaleAspectFill
        })
        if detalleMovie.productionCompanies?.count == 0{
            galleryComponent.isHidden = true
            lblTitleGallery.isHidden = true
        }else{
            galleryComponent.isHidden = false
            lblTitleGallery.isHidden = false
            galleryComponent.setImagens(imagens: getUrlImagesCompanies(detalleMovie.productionCompanies ?? []))
        }
        
    }
    
    func successSerchFav(_ movie: MoviesFav){
        self.favMovie = movie
    }
    
    func updateStatusBtn(_ isDelete: Bool){
        self.btnFavs.setImage(UIImage(named: isDelete ? "favOff" : "favOn"), for: .normal)
    }
    
    
    private func getUrlImagesCompanies(_ arrCompanies: [ProductionCompany]) -> [String]{
        var arrUrl: [String] = []
        
        for companie in arrCompanies {
            arrUrl.append("https://image.tmdb.org/t/p/w500\(companie.logoPath ?? "")")
        }
        return arrUrl
    }
    
    private func getOriginalLanguage(_ key: String) -> String{
        
        switch key {
        case "en":
            return "Ingles"
        case "es":
            return "Español"
        case "ko":
            return  "Koreano"
        case "no":
            return "Noruego"
        default:
            return "Desconocido"
        }

    }
    
    func getGeneros(_ arrGeneros: [Genre]) -> String {
        var generos = ""
        var cont = 0
        for genero in arrGeneros{
            generos += "\(cont == 0 ? "":",") \(genero.name)"
            cont += 1
        }
        return generos
    }

}
