//
//  DetalleMovieInteractor.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 17/02/23.
//

import Foundation
import UIKit

class DetalleMovieInteractor: DetalleMovieInteractorDelegate {
    var presenter:  DetalleMoviePresenterDelegate?
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    func requestDetalleMovie(_ idMovie: Int){
        let url = URL(string: "https://api.themoviedb.org/3/movie/\(idMovie)?api_key=9be77ffe6de25d45b7cedad4c1b5a499&language=es-MX")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard error == nil else{
                return
            }
            guard let data = data else {
                return
            }
            do {
                let jsonResponse = try JSONDecoder().decode(Movies.self, from: data)
                DispatchQueue.main.async {
                    self.presenter?.successDetalleMovie(jsonResponse)
                }
            } catch {
                print("Error \(error.localizedDescription)")
            }
            
        }.resume()
    }
    
    func addFavorito(_ movie: Movies) {
        let nuevoFav = MoviesFav(context: self.context)
         nuevoFav.posterPath = movie.posterPath
         nuevoFav.id = Int32(movie.id ?? 0)
         nuevoFav.voteAverage = movie.voteAverage ?? 0.0
         nuevoFav.overview = movie.overview
         nuevoFav.originalTitle = movie.originalTitle
         nuevoFav.releaseDate = movie.releaseDate
         
         try! self.context.save()
        presenter?.updateStatusBtnFav(false)

        
    }
    
    
    func eliminarFavorito(_ movie: MoviesFav){
            self.context.delete(movie)
        
            try! self.context.save()
        
        presenter?.updateStatusBtnFav(true)
    }
    
    func searchFav(_ idMovie: Int){
        do{
            let favoritos = try context.fetch(MoviesFav.fetchRequest())
            DispatchQueue.main.async {
                self.isFavSave(favoritos, idMovie: Int32(idMovie))
            }
        }catch{
            print("no se pueden recuperar los datos")
        }
    }
    
    private func isFavSave(_ arrFav: [MoviesFav], idMovie: Int32){
        let favMovie = arrFav.firstIndex {$0.id == idMovie}
        if let indexMovie = favMovie{
            let movie = arrFav[indexMovie]
            presenter?.showStatusFavMovie(movie)
            presenter?.updateStatusBtnFav(false)
        }
    }
}
