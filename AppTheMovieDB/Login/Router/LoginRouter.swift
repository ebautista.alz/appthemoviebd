//
//  LoginRouter.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 17/02/23.
//

import UIKit

class LoginRouter: LoginRouterDelegate {
        
   static func createModule() -> UIViewController {
        
        let view = LoginViewController(nibName: "LoginViewController", bundle: Bundle(for: LoginViewController.self))
        let interactor =  LoginInteractor()
        let presenter = LoginPresenter(interface: view, interactor: interactor)
       view.presenter = presenter
       interactor.presenter = presenter
        return view
    }

}
