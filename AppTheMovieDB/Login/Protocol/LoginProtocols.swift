//
//  LoginProtocols.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 17/02/23.
//

import Foundation

protocol LoginViewDelegate: AnyObject {
    var presenter: LoginPresenterDelegate? { get set }
    func showMovieList()
    func showErrorMensaje(_ mensaje:  String)
}
protocol LoginPresenterDelegate: AnyObject {
    func validarInfo(usuario: String, codigo: String)
    func successLogin()
    func errorLogin(_ msg: String)
    
}
protocol LoginInteractorDelegate: AnyObject {
    var presenter: LoginPresenterDelegate? { get set }
    func logIn(usuario: String, codigo: String)

}
protocol LoginRouterDelegate: AnyObject {}
