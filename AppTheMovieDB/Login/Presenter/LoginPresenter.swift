//
//  LoginPresenter.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 17/02/23.
//

import Foundation

class LoginPresenter: LoginPresenterDelegate {
    
    
     var view: LoginViewDelegate?
     var  interactor: LoginInteractorDelegate?

   init(interface: LoginViewDelegate, interactor: LoginInteractorDelegate) {
       self.view = interface
       self.interactor = interactor
   }
    
    func validarInfo(usuario: String, codigo: String){
        interactor?.logIn(usuario: usuario, codigo: codigo)
    }
    
    func successLogin(){
        view?.showMovieList()
    }
    
    func errorLogin(_ msg: String){
        view?.showErrorMensaje(msg)
    }
}
