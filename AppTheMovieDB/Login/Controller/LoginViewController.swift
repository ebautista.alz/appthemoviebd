//
//  LoginViewController.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 17/02/23.
//

import UIKit

class LoginViewController: UIViewController, LoginViewDelegate {
    
    @IBOutlet weak var txtCode: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var btnLogIn: UIButton!
    
    var presenter: LoginPresenterDelegate?


    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblError.text = ""
        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    @IBAction func actionLogIn(_ sender: UIButton) {
        let usuario = txtName.text ?? ""
        let codigo = txtCode.text ?? ""
        if !usuario.isEmpty && !usuario.isEmpty{
            presenter?.validarInfo(usuario: usuario, codigo: codigo)
        }else{
            lblError.text = "Campos vacios, ingresa nombre de usuario y contresaña."
        }
        
    }
    
    func showMovieList() {
            let view = MoviesRouter.createModule()
            self.navigationController?.pushViewController(view, animated: true)
    }
    
    func showErrorMensaje(_ mensaje:  String) {
        lblError.text = mensaje
    }

}

