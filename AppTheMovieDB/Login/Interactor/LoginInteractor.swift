//
//  LoginInteractor.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 17/02/23.
//

import Foundation

class LoginInteractor: LoginInteractorDelegate {
    var presenter: LoginPresenterDelegate?
    func logIn(usuario: String, codigo: String){
        
        self.crearToken { token in
            let url = URL(string: "https://api.themoviedb.org/3/authentication/token/validate_with_login?api_key=9be77ffe6de25d45b7cedad4c1b5a499")
                    
                
            let model  = RequestToken(username: usuario, password: codigo, requestToken: token)
            guard let jsonData = try? JSONEncoder().encode(model) else {
                      print("Error: Trying to convert model to JSON data")
                      return
                  }
            print(String(data: jsonData, encoding: .utf8)!)

            var request = URLRequest(url: url!)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.httpBody = jsonData
                URLSession.shared.dataTask(with: request) { data, response, error in
                    
                    guard error == nil else{
                        self.presenter?.errorLogin("")
                        return
                    }
                    guard let data = data else {
                        self.presenter?.errorLogin("ERROR: No se pudieron optener los datos.")
                        return
                    }
                    do {
                        let jsonResponse = try JSONDecoder().decode(NewToken.self, from: data)
                        DispatchQueue.main.async {
                            Sesion.shared.addInfoUser(token: jsonResponse.requestToken, usuario: usuario)
                            self.presenter?.successLogin()
                        }
                    } catch {
                        self.presenter?.errorLogin("Invalid username and/or password: You did not provide a valid login")
                    }
                    
                }.resume()
        }
        
    }
    
    func crearToken(completion: @escaping (_ token: String) -> Void){
        let url = URL(string: "https://api.themoviedb.org/3/authentication/token/new?api_key=9be77ffe6de25d45b7cedad4c1b5a499")
                
            var request = URLRequest(url: url!)
            request.httpMethod = "GET"
            URLSession.shared.dataTask(with: request) { data, response, error in
                
                guard error == nil else{
                    self.presenter?.errorLogin("")
                    return
                }
                guard let data = data else {
                    self.presenter?.errorLogin("")
                    return
                }

                do {
                    let jsonResponse = try JSONDecoder().decode(NewToken.self, from: data)
                    completion(jsonResponse.requestToken)
                } catch {
                   self.presenter?.errorLogin(error.localizedDescription)
                }
                
            }.resume()
    }
    
}
