//
//  SesionModel.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 18/02/23.
//

import Foundation

struct NewToken: Codable {
    let success: Bool
    let expiresAt, requestToken: String

    enum CodingKeys: String, CodingKey {
        case success
        case expiresAt = "expires_at"
        case requestToken = "request_token"
    }
}

struct RequestToken: Codable {
    let username, password, requestToken: String

    enum CodingKeys: String, CodingKey {
        case username, password
        case requestToken = "request_token"
    }
}
