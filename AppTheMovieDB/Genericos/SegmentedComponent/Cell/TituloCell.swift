//
//  TituloCell.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 17/02/23.
//

import UIKit

class TituloCell: UICollectionViewCell {

    @IBOutlet weak var lblTitulo: UILabel!
    @IBOutlet weak var backGround: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
