//
//  SegmentedComponent.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 17/02/23.
//

import UIKit

protocol SegmentedComponentDelegate{
    func onSelected(index: Int)
}
class SegmentedComponent: UIView {
    
    @IBOutlet weak var segmented: UICollectionView!
    private var delegate: SegmentedComponentDelegate?
    private var options: [String] = []
    private var indexSelected: Int?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func setOptions(options: [String], delegate: SegmentedComponentDelegate, selectFirst: Bool = false){
        self.indexSelected = selectFirst ? 0 : nil
        self.delegate = delegate
        self.options = options
        segmented.reloadData()
    }
    
    private func commonInit() {
        
        let nib = UINib(nibName: "SegmentedComponent", bundle: Bundle(for: SegmentedComponent.self))
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        self.collectionInit()
    }
    
    private func collectionInit() {
        
        let nibName = UINib(nibName: "TituloCell", bundle: Bundle(for: SegmentedComponent.self))
        segmented.register(nibName, forCellWithReuseIdentifier: "SegmentedComponentCell")
        segmented.allowsMultipleSelection = false
        segmented.showsHorizontalScrollIndicator = false
        segmented.showsVerticalScrollIndicator = false
        segmented.delegate = self
        segmented.dataSource = self
        if let layout = segmented.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
        }
    }
    
}

extension SegmentedComponent: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return options.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = segmented.dequeueReusableCell(withReuseIdentifier: "SegmentedComponentCell", for: indexPath) as? TituloCell{
            cell.lblTitulo.text = options[indexPath.row]
            cell.lblTitulo.textColor = .white
            
            cell.backGround.backgroundColor = .black
            
            if indexSelected == indexPath.row{
                cell.backGround.backgroundColor = .darkGray
                cell.backGround.layer.cornerRadius = 8
                cell.backGround.layer.masksToBounds = true
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.indexSelected = indexPath.row
        delegate?.onSelected(index: indexPath.row)
        segmented.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        return  CGSize(width: 100, height: 35)
        
    }
    
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }

}
