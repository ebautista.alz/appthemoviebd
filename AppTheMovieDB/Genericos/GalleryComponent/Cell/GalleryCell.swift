//
//  GalleryCell.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 20/02/23.
//

import UIKit

class GalleryCell: UICollectionViewCell {
    
    @IBOutlet weak var contentGallery: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
