//
//  GalleryComponent.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 20/02/23.
//

import UIKit

class GalleryComponent: UIView {
    
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    private var arrUrlImages: [String] = []
    private var activityTimer: Timer?
    private var cont = 0
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func setImagens(imagens: [String]){
        self.arrUrlImages = imagens
        pageControl.numberOfPages = arrUrlImages.count
        pageControl.currentPage = 0
        if arrUrlImages.count <= 1 {
            pageControl.isHidden = true
        }else{
            pageControl.isHidden = false
            self.addTimer()
        }
        collection.reloadData()
    }
    
    private func commonInit() {
        
        let nib = UINib(nibName: "GalleryComponent", bundle: Bundle(for: GalleryComponent.self))
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        self.collectionInit()
    }
    
    private func collectionInit() {
        
        let nibName = UINib(nibName: "GalleryCell", bundle: Bundle(for: GalleryComponent.self))
        collection.register(nibName, forCellWithReuseIdentifier: "GalleryCell")
        collection.allowsMultipleSelection = false
        collection.showsHorizontalScrollIndicator = false
        collection.showsVerticalScrollIndicator = false
        collection.dataSource = self
        collection.delegate = self
        if let layout = collection.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
        }
        
    }
    
    private func addTimer(){
        if self.activityTimer  == nil {
            self.activityTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(changeImage), userInfo: nil, repeats: true)
        }
    }
    
    func removeTimer(){
        guard let timer = self.activityTimer else { return }
        
        timer.invalidate()
        self.activityTimer = nil
    }
    
    @objc private func changeImage(){
        let indexPath = IndexPath(item: cont, section: 0)
        collection.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
        if cont == (arrUrlImages.count - 1){
            cont = 0
        }else{
            cont += 1
        }
    }
}

extension GalleryComponent: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrUrlImages.count
    }
    
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collection.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath) as? GalleryCell{
            let url = arrUrlImages[indexPath.row]
            Utils.getImageFromUrl(url) { image in
                cell.contentGallery.image = image
            }
            return cell
        }
        return UICollectionViewCell()
    }

     func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        return  CGSize(width: self.frame.size.width, height: self.frame.size.height)
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        pageControl.currentPage = indexPath.row
    }
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}
