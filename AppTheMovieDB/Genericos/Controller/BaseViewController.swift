//
//  BaseViewController.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 17/02/23.
//

import UIKit

class BaseViewController: UIViewController {

    private var loaderAlert: UIAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        changeNavigationBarColor()
    }
    
    func setProfileButton(){
        if let helpImage = UIImage(named: "menuIcon")?.withRenderingMode(.alwaysTemplate) {
            let helpButton = UIBarButtonItem(image: helpImage,
                                             style: .plain, target: self, action: #selector(showMenu))


            helpButton.tintColor = UIColor.white
            helpButton.customView?.tintColor = UIColor.white
            self.navigationItem.rightBarButtonItems = [helpButton]
        }
    }
    
    @objc func showMenu(){
        
    }
    
    func changeNavigationBarColor(){
        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.configureWithDefaultBackground()
        navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        navBarAppearance.backgroundColor = UIColor.blueDark

        navigationItem.standardAppearance = navBarAppearance
        navigationItem.compactAppearance = navBarAppearance
        navigationItem.scrollEdgeAppearance = navBarAppearance
    }
    func showBackButton(){
        
        let backBtn = UIButton()
        backBtn.setImage(UIImage(named: "backArrow"), for: UIControl.State())
        backBtn.addTarget(self, action:#selector(goBack), for:.touchUpInside)
        let leftBarButtonItem = UIBarButtonItem(customView: backBtn)
        navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    

     @objc func goBack() {
         self.navigationController?.popViewController(animated: true)
     }
    
}
