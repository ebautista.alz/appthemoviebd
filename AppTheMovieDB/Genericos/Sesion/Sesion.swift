//
//  Sesion.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 19/02/23.
//

import Foundation

class Sesion {
    
    static let shared = Sesion()
    
    var token: String?
    var usuario: String?
    
    
    func addInfoUser(token: String, usuario: String){
        self.token = token
        self.usuario = usuario
    }
    
    func resetLoginData(){
        self.token = nil
        self.usuario = nil
    }

}
