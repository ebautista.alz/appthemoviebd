//
//  ProfileInteractor.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 17/02/23.
//

import Foundation
import UIKit

class ProfileInteractor: ProfileInteractorDelegate{
    var presenter: ProfilePresenterDelegate?
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    func getFavMovies(){
        do{
            let arrfavoritos = try context.fetch(MoviesFav.fetchRequest())
            DispatchQueue.main.async {
                self.addPathImages(arrfavoritos)
            }
        }catch{
            print("Error al consultar los datos")
        }
    }
    
    private func addPathImages(_ listMovies: [MoviesFav]){
        var arrMovies = listMovies
        for index in 0..<arrMovies.count{
            let posterPath = arrMovies[index].posterPath
            arrMovies[index].posterPath = "https://image.tmdb.org/t/p/w500\(posterPath ?? "")"
        }
        presenter?.succesSearchFavMovies(arrMovies)
    }
}
