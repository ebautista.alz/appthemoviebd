//
//  ProfilePresenter.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 17/02/23.
//

import Foundation

class ProfilePresenter: ProfilePresenterDelegate {
  
    
    
    var view: ProfileViewDelegate?
    var interactor: ProfileInteractorDelegate?

    init(interface: ProfileViewDelegate, interactor: ProfileInteractorDelegate) {
        self.view = interface
        self.interactor = interactor
    }
    func searchFavMovies(){
        interactor?.getFavMovies()
    }
    
    func succesSearchFavMovies(_ arrFavs: [MoviesFav]){
        view?.successSearchFav(arrFavs)
    }

}
