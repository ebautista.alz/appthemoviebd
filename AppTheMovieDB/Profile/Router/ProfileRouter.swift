//
//  ProfileRouter.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 17/02/23.
//

import UIKit

class ProfileRouter: ProfileRouterDelegate {
        
    static func createModule() -> UIViewController {
        let view = ProfileViewController(nibName: "ProfileViewController", bundle: Bundle(for: ProfileViewController.self))
        let interactor = ProfileInteractor()
        let presenter = ProfilePresenter(interface: view, interactor: interactor)
        view.presenter = presenter
        interactor.presenter = presenter
        return view
    }
}
