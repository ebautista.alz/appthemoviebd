//
//  ProfileProtocol.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 17/02/23.
//

import Foundation

protocol  ProfileViewDelegate: AnyObject {
    var presenter: ProfilePresenterDelegate? { get set }
    func successSearchFav(_ arrFavs: [MoviesFav])
}
protocol  ProfilePresenterDelegate: AnyObject {
    func searchFavMovies()
    func succesSearchFavMovies(_ arrFavs: [MoviesFav])
}
protocol  ProfileInteractorDelegate: AnyObject {
    var presenter: ProfilePresenterDelegate? { get set }
    func getFavMovies()
}
protocol  ProfileRouterDelegate: AnyObject {}
