//
//  ProfileViewController.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 17/02/23.
//

import UIKit

class ProfileViewController: UIViewController {
    
    var presenter: ProfilePresenterDelegate?

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblUser: UILabel!
    @IBOutlet weak var lblTitleFavs: UILabel!

    @IBOutlet weak var favCollection: UICollectionView!
    
    private var favoritos: [MoviesFav] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.textColor = .greenLime
        lblTitleFavs.textColor = .greenLime
        lblUser.textColor = .greenLime
        lblUser.text = Sesion.shared.usuario ?? ""
        // Do any additional setup after loading the view.
        self.collectionInit()
        presenter?.searchFavMovies()
    }

    private func collectionInit() {

        let nibName = UINib(nibName: "MoviesCell", bundle: Bundle(for: MoviesViewController.self))
        favCollection.register(nibName, forCellWithReuseIdentifier: "MoviesCell")
        favCollection.allowsMultipleSelection = false
        favCollection.showsHorizontalScrollIndicator = false
        favCollection.showsVerticalScrollIndicator = false
        favCollection.delegate = self
        favCollection.dataSource = self
        
    }
    
}

extension ProfileViewController: ProfileViewDelegate {
    func successSearchFav(_ arrFavs: [MoviesFav]){
        self.favoritos = arrFavs
        favCollection.reloadData()
    }
}


extension ProfileViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return favoritos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MoviesCell", for: indexPath) as? MoviesCell {
            let movie = favoritos[indexPath.row]
            cell.backgroundColor = .blueDark
            cell.layer.masksToBounds = true
            cell.layer.cornerRadius = 10

            cell.lblTitulo.text = movie.originalTitle
            cell.lblTitulo.numberOfLines = 2
            cell.lblTitulo.textColor = UIColor.greenLime
            let icon = UIImage(named: "startRate") ?? UIImage()
            cell.lblCalificacion.attributedText = Utils.addIconLabel(image: icon, text: " \(String(describing: movie.voteAverage))")
            cell.lblCalificacion.textColor = UIColor.greenLime

            cell.lblDescripcion.text = movie.overview
            cell.lblDescripcion.textColor = .white
            
            cell.lblFecha.text = movie.releaseDate
            cell.lblFecha.textColor = UIColor.greenLime
            cell.btnFavorito.isHidden = true
            Utils.getImageFromUrl("\(movie.posterPath ?? "")", completion: { image in
                 cell.imgPortada.image = image
                cell.imgPortada.contentMode = .scaleAspectFill
                cell.imgPortada.layer.masksToBounds = true
                cell.imgPortada.layer.cornerRadius = 10
            })
            return cell
        }
        return UICollectionViewCell()
    }

    
    func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        let withCell = (collectionView.frame.width - 20)/2
        return  CGSize(width: withCell, height: (withCell * 2))
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 15.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15.0
    }
    
}

