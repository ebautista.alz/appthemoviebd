//
//  MoviesFav+CoreDataProperties.swift
//  
//
//  Created by Enrique Bautista Alvarez on 20/02/23.
//
//

import Foundation
import CoreData


extension MoviesFav {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MoviesFav> {
        return NSFetchRequest<MoviesFav>(entityName: "MoviesFav")
    }

    @NSManaged public var voteAverage: Double
    @NSManaged public var overview: String?
    @NSManaged public var posterPath: String?
    @NSManaged public var releaseDate: String?
    @NSManaged public var originalTitle: String?
    @NSManaged public var id: Int32

}
