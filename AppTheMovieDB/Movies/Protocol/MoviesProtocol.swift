//
//  MoviesProtocol.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 17/02/23.
//

import Foundation

protocol MoviesViewDelegate: AnyObject {
    var presenter: MoviesPresenterDelegate? { get set }
    func showListMovies(_ lista: ListMovies)
    func showError(_ msg: String)
    func showCategorias(_ arrCategorias: [Categorias])

}
protocol MoviesPresenterDelegate: AnyObject {
    func getListMovies(_ categoria: String)
    func successListMovies(_ listMovies: ListMovies)
    func errorListMovies(_ msg: String)
    func successListCategorias(_ arrCategorias: [Categorias])
    func getListaCategorias()

}
protocol MoviesInteractorDelegate: AnyObject {
    var presenter: MoviesPresenterDelegate? { get set }
    func requestObtenerListaPeliculas(_ categoria: String)
    func getCategorias()
}
protocol MoviesRouterDelegate: AnyObject {}
