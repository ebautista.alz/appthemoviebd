//
//  MoviesPresenter.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 17/02/23.
//

import Foundation

class MoviesPresenter: MoviesPresenterDelegate {
    
    var view: MoviesViewDelegate?
    var interactor: MoviesInteractorDelegate?

    init(interface: MoviesViewDelegate, interactor: MoviesInteractorDelegate) {
        self.view = interface
        self.interactor = interactor
    }
    
    
    func getListMovies(_ categoria: String){
        interactor?.requestObtenerListaPeliculas(categoria)
    }

    func successListMovies(_ listMovies: ListMovies){
        self.view?.showListMovies(listMovies)
    }

    func errorListMovies(_ msg: String){
        
    }
    
    func successListCategorias(_ arrCategorias: [Categorias]) {
        view?.showCategorias(arrCategorias)
    }
    
    func getListaCategorias() {
        interactor?.getCategorias()
    }
}
