//
//  MoviesCell.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 17/02/23.
//

import UIKit

class MoviesCell: UICollectionViewCell {
    
    @IBOutlet weak var lblDescripcion: UILabel!
    @IBOutlet weak var lblTitulo: UILabel!
    @IBOutlet weak var lblFecha: UILabel!
    @IBOutlet weak var lblCalificacion: UILabel!
    @IBOutlet weak var imgPortada: UIImageView!
    @IBOutlet weak var btnFavorito: UIButton!

    
    
}
