//
//  MoviesViewController.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 17/02/23.
//

import UIKit

class MoviesViewController: BaseViewController {
    
    @IBOutlet weak var moviesList: UICollectionView!
    @IBOutlet weak var contentOptions: SegmentedComponent!

    var presenter: MoviesPresenterDelegate?
    private var arrMovies: [Movies] = []
    private var arrCategorias: [Categorias] = []
    private let loader = Utils.showLoader()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "TV Shows"
        self.collectionInit()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.setHidesBackButton(true, animated: true)
        setProfileButton()
        presenter?.getListaCategorias()
    }
    


    private func collectionInit() {

        let nibName = UINib(nibName: "MoviesCell", bundle: Bundle(for: MoviesViewController.self))
        moviesList.register(nibName, forCellWithReuseIdentifier: "MoviesCell")
        moviesList.allowsMultipleSelection = false
        moviesList.showsHorizontalScrollIndicator = false
        moviesList.showsVerticalScrollIndicator = false
        moviesList.delegate = self
        moviesList.dataSource = self
        
    }
    
   override func showMenu(){
       
       let alerta = UIAlertController(title: "",
                                      message: "What do you want to do?",
                                      preferredStyle: .actionSheet)
      
       let actOne = UIAlertAction(title: "View Profile", style: .default){_ in
           let view = ProfileRouter.createModule()
           self.modalPresentationStyle = .currentContext
           self.present(view, animated: true)
       }
       let actTwo = UIAlertAction(title: "Log out", style: .destructive){_ in
           Sesion.shared.resetLoginData()
           self.navigationController?.popToRootViewController(animated: true)
       }
       let actThree = UIAlertAction(title: "Cancel", style: .cancel)
       alerta.addAction(actOne)
       alerta.addAction(actTwo)
       alerta.addAction(actThree)
       self.present(alerta, animated: true, completion: nil)

    }

}

extension MoviesViewController: SegmentedComponentDelegate{
    func onSelected(index: Int) {
        let categoria = self.arrCategorias[index]
        presenter?.getListMovies(categoria.url ?? "")
    }

}

extension MoviesViewController: MoviesViewDelegate{
    
    func showCategorias(_ arrCategorias: [Categorias]) {
        self.arrCategorias = arrCategorias
        var tituloCategoria: [String] = []
        for titulo in self.arrCategorias {
            tituloCategoria.append(titulo.title ?? "")
        }
        contentOptions.setOptions(options: tituloCategoria, delegate: self, selectFirst: true)
        presenter?.getListMovies(self.arrCategorias.first?.url ?? "")

    }
    
    func showListMovies(_ lista: ListMovies){
        self.arrMovies = lista.results ?? []
        self.moviesList.reloadData()
    }
    
    func showError(_ msg: String){
    }
}

extension MoviesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrMovies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MoviesCell", for: indexPath) as? MoviesCell {
            let movie = arrMovies[indexPath.row]
            cell.backgroundColor = .blueDark
            cell.layer.masksToBounds = true
            cell.layer.cornerRadius = 10

            cell.lblTitulo.text = movie.originalTitle
            cell.lblTitulo.numberOfLines = 2
            cell.lblTitulo.textColor = UIColor.greenLime
            let icon = UIImage(named: "startRate") ?? UIImage()
            cell.lblCalificacion.attributedText = Utils.addIconLabel(image: icon, text: " \(String(describing: movie.voteAverage ?? 0.0))")
            cell.lblCalificacion.textColor = UIColor.greenLime

            cell.lblDescripcion.text = movie.overview
            cell.lblDescripcion.textColor = .white
            
            cell.lblFecha.text = movie.releaseDate
            cell.lblFecha.textColor = UIColor.greenLime

            Utils.getImageFromUrl("\(movie.posterPath ?? "")", completion: { image in
                 cell.imgPortada.image = image
                cell.imgPortada.contentMode = .scaleAspectFill
                cell.imgPortada.layer.masksToBounds = true
                cell.imgPortada.layer.cornerRadius = 10
            })
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let id = arrMovies[indexPath.row].id ?? 0
        let view = DetalleMovieRouter.createModule(idMovie: id)
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    public func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        let withCell = (collectionView.frame.width - 20)/2
        return  CGSize(width: withCell, height: (withCell * 2))
        
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 15.0
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15.0
    }
    
}
