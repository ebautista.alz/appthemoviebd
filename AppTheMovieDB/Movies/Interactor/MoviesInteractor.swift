//
//  MoviesInteractor.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 17/02/23.
//

import UIKit

class MoviesInteractor: MoviesInteractorDelegate {
    
    var presenter: MoviesPresenterDelegate?
    

    
    func requestObtenerListaPeliculas(_ categoria: String){
        let url = URL(string: "https://api.themoviedb.org/3/movie/\(categoria)?api_key=9be77ffe6de25d45b7cedad4c1b5a499&language=es-MX&page=1")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard error == nil else{
                self.presenter?.errorListMovies("error")
                return
            }
            guard let data = data else {
                self.presenter?.errorListMovies("Sin información")
                return
            }
            do {
                let jsonResponse = try JSONDecoder().decode(ListMovies.self, from: data)
                DispatchQueue.main.async {
                    self.addPathImages(jsonResponse)
                }
            } catch {
                print("Error \(error.localizedDescription)")
            }
            
        }.resume()
    }
    
    func getCategorias(){
       
        let categories = [ Categorias(title: "Now Playing", url: "now_playing"),
                           Categorias(title: "Popular", url: "popular"),
                           Categorias(title: "Top Rated", url: "top_rated"),
                           Categorias(title: "Upcoming", url: "upcoming")]
        presenter?.successListCategorias(categories)
    }
    
    private func addPathImages(_ listMovies: ListMovies){
        var arrMovies = listMovies
        for index in 0..<(arrMovies.results ?? []).count{
            let posterPath = arrMovies.results?[index].posterPath
            arrMovies.results?[index].posterPath = "https://image.tmdb.org/t/p/w500\(posterPath ?? "")"
        }
        self.presenter?.successListMovies(arrMovies)
    }
    
    
    func saveFavMovie(_ movie: Movies){
        
    }
}
