//
//  Colors.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 17/02/23.
//

import UIKit

extension UIColor {
    
    class var negro: UIColor {
        return .black
    }
    
    class var blanco: UIColor {
        return .white
    }
    
    class var blueDark: UIColor {
        return UIColor(red: 0.11, green: 0.15, blue: 0.17, alpha: 1.0)
    }
    
    class var rojo: UIColor {
        return .red
    }
    class var dimGray: UIColor {
        return UIColor(red: 0.19, green: 0.21, blue: 0.22, alpha: 1.0)
    }
    class var greenLime: UIColor {
        return UIColor(red: 0.45, green: 0.71, blue: 0.44, alpha: 1.0)
    }
}
