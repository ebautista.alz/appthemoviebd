//
//  Utils.swift
//  AppTheMovieDB
//
//  Created by Enrique Bautista Alvarez on 19/02/23.
//

import UIKit
import SDWebImage


class Utils{

static func getImageFromUrl(_ urlString:String,completion: @escaping (_ image: UIImage) -> ()){
     let imageView =  UIImageView()
         imageView.sd_setImage(with: URL(string: urlString), placeholderImage: nil, options:[.continueInBackground, .lowPriority, .refreshCached, .handleCookies, .retryFailed] , completed:  { (image, error, cacheType, url) in
             if let message = error {
                 print("Failed: \(message.localizedDescription)")
             } else {
                 completion(imageView.image!)
             }
         })
 }
     static func addIconLabel(image: UIImage, text:String) -> NSMutableAttributedString{
         let attachment = NSTextAttachment()
                attachment.image = image

                let attachmentString = NSAttributedString(attachment: attachment)
                let mutableAttributedString = NSMutableAttributedString()
                mutableAttributedString.append(attachmentString)
                
                let string = NSMutableAttributedString(string: text, attributes: [:])
                mutableAttributedString.append(string)
                return mutableAttributedString
        }
    
   static func showLoader() -> UIAlertController {
        let loader = UIAlertController(title: nil, message: "Cargando...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.large
        loadingIndicator.startAnimating()
        loader.view.addSubview(loadingIndicator)
        return loader
    }
    
    static func stopLoader(_ loader: UIAlertController) {
        DispatchQueue.main.async {
            loader.view.removeFromSuperview()
            loader.dismiss(animated: true, completion: nil)
        }
    }
    
}


